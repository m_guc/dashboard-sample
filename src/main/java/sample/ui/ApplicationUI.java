package sample.ui;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.VaadinUI;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.events.EventScope;

import sample.ui.event.ConfigureNavigatorEvent;
import sample.ui.presenter.MainPresenter;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

@Theme("dashboard")
@VaadinUI(path = "/main")
@Title("Spring-Vaadin MVP Demo")
// @PreserveOnRefresh
public class ApplicationUI extends UI {

	@Autowired
	private EventBus eventBus;

	@Autowired
	private MainPresenter mainPresenter;

	@Override
	protected void init(VaadinRequest request) {
		setSizeFull();
		setContent(mainPresenter.getView());
		Responsive.makeResponsive(this);
		eventBus.publish(EventScope.UI, this, new ConfigureNavigatorEvent(this));

	}

}
