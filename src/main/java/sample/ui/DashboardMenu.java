package sample.ui;

import sample.ui.event.DashboardEvent.NotificationsCountUpdatedEvent;
import sample.ui.event.DashboardEvent.PostViewChangeEvent;
import sample.ui.event.DashboardEvent.ReportsCountUpdatedEvent;

import com.google.gwt.thirdparty.guava.common.eventbus.Subscribe;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

/**
 * A responsive menu component providing user information and the controls for
 * primary navigation between the views.
 */
@SuppressWarnings({ "serial", "unchecked" })
public final class DashboardMenu extends CustomComponent {

	public static final String ID = "dashboard-menu";
	public static final String REPORTS_BADGE_ID = "dashboard-menu-reports-badge";
	public static final String NOTIFICATIONS_BADGE_ID = "dashboard-menu-notifications-badge";
	private static final String STYLE_VISIBLE = "valo-menu-visible";
	private static final String STYLE_SELECTED = "selected";

	private Label notificationsBadge;

	private Label reportsBadge;
	private MenuItem settingsItem;
	private CssLayout menu;

	public DashboardMenu() {
		addStyleName("valo-menu");
		setId(ID);
		setSizeUndefined();

		// There's only one DashboardMenu per UI so this doesn't need to be
		// unregistered from the UI-scoped DashboardEventBus.

		// TODO register as listener
		// DashboardEventBus.register(this);

		setCompositionRoot(buildContent());
	}

	private Component buildContent() {
		final CssLayout menuContent = new CssLayout();
		menuContent.addStyleName("sidebar");
		menuContent.addStyleName(ValoTheme.MENU_PART);
		menuContent.addStyleName("no-vertical-drag-hints");
		menuContent.addStyleName("no-horizontal-drag-hints");
		menuContent.setWidth(null);
		menuContent.setHeight("100%");

		menuContent.addComponent(buildTitle());
		menuContent.addComponent(buildUserMenu());
		menuContent.addComponent(buildToggleButton());
		menuContent.addComponent(getMenu());

		return menuContent;
	}

	private Component buildTitle() {
		Label logo = new Label("Sample <strong>Dashboard</strong>", ContentMode.HTML);
		logo.setSizeUndefined();
		HorizontalLayout logoWrapper = new HorizontalLayout(logo);
		logoWrapper.setComponentAlignment(logo, Alignment.MIDDLE_CENTER);
		logoWrapper.addStyleName("valo-menu-title");
		return logoWrapper;
	}

	private Component buildUserMenu() {
		final MenuBar settings = new MenuBar();
		settings.addStyleName("user-menu");

		settingsItem = settings.addItem("", new ThemeResource("img/profile-pic-300px.jpg"), null);

		// TODO set default username
		updateUserName("Sample Username");

		settingsItem.addItem("Edit Profile", new Command() {
			@Override
			public void menuSelected(final MenuItem selectedItem) {
				// TODO open edit profile window
				// ProfilePreferencesWindow.open(user, false);
			}
		});
		settingsItem.addItem("Preferences", new Command() {
			@Override
			public void menuSelected(final MenuItem selectedItem) {
				// TODO open preferences window
				// ProfilePreferencesWindow.open(user, true);
			}
		});
		settingsItem.addSeparator();
		settingsItem.addItem("Sign Out", new Command() {
			@Override
			public void menuSelected(final MenuItem selectedItem) {
				// TODO logout event
			}
		});
		return settings;
	}

	private Component buildToggleButton() {
		Button valoMenuToggleButton = new Button("Menu", new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				if (getCompositionRoot().getStyleName().contains(STYLE_VISIBLE)) {
					getCompositionRoot().removeStyleName(STYLE_VISIBLE);
				} else {
					getCompositionRoot().addStyleName(STYLE_VISIBLE);
				}
			}
		});
		valoMenuToggleButton.setIcon(FontAwesome.LIST);
		valoMenuToggleButton.addStyleName("valo-menu-toggle");
		valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
		valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_SMALL);
		return valoMenuToggleButton;
	}

	private CssLayout getMenu() {
		if (menu == null) {
			menu = new CssLayout();
			menu.addStyleName("valo-menuitems");
			menu.setHeight(100.0f, Unit.PERCENTAGE);
		}
		return menu;
	}

	public void addMenuItem(String viewName, String title, Resource icon) {
		Button button = new Button(title);
		button.setData(viewName);
		button.setPrimaryStyleName("valo-menu-item");
		button.setIcon(icon);
		button.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				UI.getCurrent().getNavigator().navigateTo(viewName);
			}
		});
		getMenu().addComponent(button);
	}

	public void selectMenuItem(String viewName) {
		// TODO fire PostViewChangeEvent somewhere

		for (Component component : menu) {
			Button button = (Button) component;
			button.removeStyleName(STYLE_SELECTED);
			if (viewName != null && viewName.equalsIgnoreCase(String.valueOf(button.getData()))) {
				button.addStyleName(STYLE_SELECTED);
			}
		}
	}

	private Component buildBadgeWrapper(final Component menuItemButton, final Component badgeLabel) {
		CssLayout dashboardWrapper = new CssLayout(menuItemButton);
		dashboardWrapper.addStyleName("badgewrapper");
		dashboardWrapper.addStyleName(ValoTheme.MENU_ITEM);
		dashboardWrapper.setWidth(100.0f, Unit.PERCENTAGE);
		badgeLabel.addStyleName(ValoTheme.MENU_BADGE);
		badgeLabel.setWidthUndefined();
		badgeLabel.setVisible(false);
		dashboardWrapper.addComponent(badgeLabel);
		return dashboardWrapper;
	}

	@Override
	public void attach() {
		super.attach();
		updateNotificationsCount(null);
	}

	@Subscribe
	public void postViewChange(final PostViewChangeEvent event) {
		// After a successful view change the menu can be hidden in mobile view.
		getCompositionRoot().removeStyleName(STYLE_VISIBLE);
	}

	@Subscribe
	public void updateNotificationsCount(final NotificationsCountUpdatedEvent event) {
		// int unreadNotificationsCount =
		// DashboardUI.getDataProvider().getUnreadNotificationsCount();
		// notificationsBadge.setValue(String.valueOf(unreadNotificationsCount));
		// notificationsBadge.setVisible(unreadNotificationsCount > 0);
	}

	@Subscribe
	public void updateReportsCount(final ReportsCountUpdatedEvent event) {
		reportsBadge.setValue(String.valueOf(event.getCount()));
		reportsBadge.setVisible(event.getCount() > 0);
	}

	public void updateUserName(String username) {
		// User user = getCurrentUser();
		settingsItem.setText(username);
	}
}
