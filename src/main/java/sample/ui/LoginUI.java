package sample.ui;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.VaadinUI;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.events.EventScope;

import sample.ui.event.CheckUserAuthenticatedEvent;
import sample.ui.presenter.LoginPresenter;
import sample.ui.view.LoginView;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

@VaadinUI(path = "/login")
@Theme("dashboard")
@Title("Login")
public class LoginUI extends UI {

	@Autowired
	private LoginView loginView;

	@Autowired
	private LoginPresenter loginPresenter;

	@Autowired
	private EventBus eventBus;

	@Override
	protected void init(VaadinRequest request) {
		eventBus.publish(EventScope.SESSION, this, new CheckUserAuthenticatedEvent());
	}

	public void showLogin() {
		setContent(loginView);
	}

}
