package sample.ui.view;

import org.vaadin.spring.UIScope;
import org.vaadin.spring.navigator.VaadinView;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.VerticalLayout;

@UIScope
@VaadinView(name = TaskView.NAME)
@ViewMetadata(icon = FontAwesome.TASKS, name = TaskView.NAME, title = "Tasks")
public class TaskView extends VerticalLayout implements View {

	public static final String NAME = "tasks";

	public TaskView() {

	}

	@Override
	public void enter(ViewChangeEvent event) {

	}

}
