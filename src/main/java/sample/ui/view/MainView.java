package sample.ui.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.UIScope;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.navigator.VaadinView;

import sample.ui.DashboardMenu;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;

@UIScope
@VaadinView(name = MainView.NAME)
public class MainView extends HorizontalLayout implements View {

	public static final String NAME = "mainView";

	@Autowired
	EventBus eventBus;

	private ComponentContainer contentContainer;

	private DashboardMenu dashboardMenu;

	public MainView() {
		setSizeFull();
		addStyleName("mainview");

		addComponent(getDashboardMenu());

		addComponent(getContentContainer());
		setExpandRatio(getContentContainer(), 1.0f);
		// DashboardNavigator(content);
		// dashboardNavigator.addProvider(springViewProvider);
	}

	public DashboardMenu getDashboardMenu() {
		if (dashboardMenu == null) {
			dashboardMenu = new DashboardMenu();
		}
		return dashboardMenu;
	}

	public ComponentContainer getContentContainer() {
		if (contentContainer == null) {
			contentContainer = new CssLayout();
			contentContainer.addStyleName("view-content");
			contentContainer.setSizeFull();
		}
		return contentContainer;
	}

	@Override
	public void enter(ViewChangeEvent event) {

	}
}
