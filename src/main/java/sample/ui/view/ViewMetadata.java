package sample.ui.view;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.vaadin.server.FontAwesome;

@Target({ java.lang.annotation.ElementType.TYPE })
@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@Documented
public @interface ViewMetadata {

	public FontAwesome icon() default FontAwesome.TABLE;

	public String name();

	public String title();

	public int order() default 0;

	public boolean initial() default true;
}
