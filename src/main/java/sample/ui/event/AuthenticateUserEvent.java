package sample.ui.event;

public class AuthenticateUserEvent {

	private final String username;
	private final String password;

	public AuthenticateUserEvent(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public static class AuthenticateUserEventFilter extends DefaultEventBusListenerMethodFilter {

		public AuthenticateUserEventFilter() {
			super(AuthenticateUserEvent.class);
		}

	}
}
