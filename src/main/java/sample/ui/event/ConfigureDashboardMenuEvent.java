package sample.ui.event;

public class ConfigureDashboardMenuEvent {

	public static class ConfigureDashboardMenuEventFilter extends DefaultEventBusListenerMethodFilter {

		public ConfigureDashboardMenuEventFilter() {
			super(ConfigureDashboardMenuEvent.class);
		}

	}
}
