package sample.ui.event;

public class CheckUserAuthenticatedEvent {

	public static class CheckUserAuthenticatedEventFilter extends DefaultEventBusListenerMethodFilter {

		public CheckUserAuthenticatedEventFilter() {
			super(CheckUserAuthenticatedEvent.class);
		}
	}

}
