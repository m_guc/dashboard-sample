package sample.ui.event;

import com.vaadin.server.Page.UriFragmentChangedEvent;

public class UriFragmentChangedEventFilter extends DefaultEventBusListenerMethodFilter {

	public UriFragmentChangedEventFilter() {
		super(UriFragmentChangedEvent.class);
	}

}
