package sample.ui.event;

public class ShowMessageEvent {

	private final String message;

	public ShowMessageEvent(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public static class ShowMessageFilter extends DefaultEventBusListenerMethodFilter {

		public ShowMessageFilter() {
			super(ShowMessageEvent.class);
		}

	}
}
