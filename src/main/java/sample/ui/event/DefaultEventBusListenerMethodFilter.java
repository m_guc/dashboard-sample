package sample.ui.event;

import org.vaadin.spring.events.EventBusListenerMethodFilter;

public class DefaultEventBusListenerMethodFilter implements EventBusListenerMethodFilter {

	private final Class<?> clazz;

	public DefaultEventBusListenerMethodFilter(Class<?> clazz) {
		this.clazz = clazz;
	}

	@Override
	public boolean filter(Object payload) {
		if (payload == null) {
			return false;
		}

		return clazz.isAssignableFrom(payload.getClass());
	}

}
