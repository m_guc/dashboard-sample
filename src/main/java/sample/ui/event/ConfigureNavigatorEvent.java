package sample.ui.event;

import com.vaadin.ui.UI;

public class ConfigureNavigatorEvent {

	private final UI vaadinUI;

	public ConfigureNavigatorEvent(UI vaadinUI) {
		this.vaadinUI = vaadinUI;
	}

	public UI getVaadinUI() {
		return vaadinUI;
	}

	public static class ConfigureNavigatorEventFilter extends DefaultEventBusListenerMethodFilter {

		public ConfigureNavigatorEventFilter() {
			super(ConfigureNavigatorEvent.class);
		}

	}
}
