package sample.ui.presenter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.vaadin.spring.events.Event;
import org.vaadin.spring.events.EventBusListenerMethod;
import org.vaadin.spring.events.EventScope;
import org.vaadin.spring.navigator.Presenter;
import org.vaadin.spring.navigator.VaadinPresenter;

import sample.ui.LoginUI;
import sample.ui.event.AuthenticateUserEvent;
import sample.ui.event.AuthenticateUserEvent.AuthenticateUserEventFilter;
import sample.ui.event.CheckUserAuthenticatedEvent;
import sample.ui.event.CheckUserAuthenticatedEvent.CheckUserAuthenticatedEventFilter;
import sample.ui.view.LoginView;

import com.vaadin.server.Page;
import com.vaadin.ui.UI;

@VaadinPresenter(viewName = LoginView.NAME)
public class LoginPresenter extends Presenter<LoginView> {

	@Autowired
	private AuthenticationManagerBuilder builder;

	@EventBusListenerMethod(scope = EventScope.SESSION, filter = CheckUserAuthenticatedEventFilter.class)
	public void checkUserAuthenticated(Event<CheckUserAuthenticatedEvent> event) {
		SecurityContext context = SecurityContextHolder.getContext();
		Authentication authentication = context.getAuthentication();

		if (authentication instanceof UsernamePasswordAuthenticationToken) {
			Page.getCurrent().setLocation("main");
		} else {
			((LoginUI) UI.getCurrent()).showLogin();
		}
	}

	@EventBusListenerMethod(scope = EventScope.SESSION, filter = AuthenticateUserEventFilter.class)
	public void authenticateUser(Event<AuthenticateUserEvent> event) {
		String username = event.getPayload().getUsername();
		String password = event.getPayload().getPassword();

		try {
			UserDetailsService userDetailsService = builder.getDefaultUserDetailsService();
			UserDetails userDetails = userDetailsService.loadUserByUsername(username);
			Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, password,
					userDetails.getAuthorities());
			SecurityContextHolder.getContext().setAuthentication(authentication);
			Page.getCurrent().setLocation("main");
		} catch (Exception e) {
			getView().showErrorNotification();
		}
	}
}
