package sample.ui.presenter;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.vaadin.spring.events.Event;
import org.vaadin.spring.events.EventBusListenerMethod;
import org.vaadin.spring.events.EventScope;
import org.vaadin.spring.navigator.Presenter;
import org.vaadin.spring.navigator.SpringViewProvider;
import org.vaadin.spring.navigator.VaadinPresenter;

import sample.ui.DashboardMenu;
import sample.ui.event.ConfigureNavigatorEvent;
import sample.ui.event.ConfigureNavigatorEvent.ConfigureNavigatorEventFilter;
import sample.ui.event.ShowMessageEvent;
import sample.ui.event.ShowMessageEvent.ShowMessageFilter;
import sample.ui.event.UriFragmentChangedEventFilter;
import sample.ui.view.MainView;
import sample.ui.view.ViewMetadata;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page.UriFragmentChangedEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

@VaadinPresenter(viewName = MainView.NAME)
public class MainPresenter extends Presenter<MainView> implements ViewChangeListener {

	@Autowired
	private SpringViewProvider springViewProvider;

	@Autowired
	private ApplicationContext applicationContext;

	@EventBusListenerMethod(scope = EventScope.SESSION, filter = ShowMessageFilter.class)
	public void onShowMessage(Event<ShowMessageEvent> event) {
		Notification.show("The Message : " + event.getPayload().getMessage());
	}

	@EventBusListenerMethod(scope = EventScope.SESSION, filter = UriFragmentChangedEventFilter.class)
	public void onUriFragmentChanged(Event<UriFragmentChangedEvent> event) {

	}

	@EventBusListenerMethod(scope = EventScope.UI, filter = ConfigureNavigatorEventFilter.class)
	public void onConfigureNavigator(Event<ConfigureNavigatorEvent> event) {

		DashboardMenu dashboardMenu = getView().getDashboardMenu();

		String initialViewName = null;

		Collection<Object> values = applicationContext.getBeansWithAnnotation(ViewMetadata.class).values();
		for (Object object : values) {
			ViewMetadata viewMetadata = object.getClass().getAnnotation(ViewMetadata.class);

			String name = viewMetadata.name();
			FontAwesome icon = viewMetadata.icon();
			String title = viewMetadata.title();
			// int order = viewMetadata.order();
			initialViewName = viewMetadata.initial() ? name : null;
			dashboardMenu.addMenuItem(name, title, icon);
		}

		UI vaadinUI = event.getPayload().getVaadinUI();
		Navigator navigator = new Navigator(vaadinUI, getView().getContentContainer());
		navigator.addProvider(springViewProvider);
		navigator.addViewChangeListener(this);
		vaadinUI.setNavigator(navigator);

		if (initialViewName == null || initialViewName.isEmpty()) {
			throw new RuntimeException("Initial view name cannot be empty, you should specify an initial view at least");
		}

		String state = navigator.getState();

		if (state == null || state.isEmpty()) {
			navigator.navigateTo(initialViewName);
		}

	}

	@Override
	public boolean beforeViewChange(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void afterViewChange(ViewChangeEvent event) {
		String viewName = event.getViewName();
		if (viewName == null) {
			return;
		}

		DashboardMenu dashboardMenu = getView().getDashboardMenu();
		// dashboardMenu.selectMenuItem(viewName);
		dashboardMenu.selectMenuItem(event.getViewName());
	}
}
