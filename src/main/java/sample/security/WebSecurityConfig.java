package sample.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

@Configuration
@EnableWebMvcSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests()
				.antMatchers("/VAADIN/**", "/ui/PUSH/**", "/ui/UIDL/**", "/ui/login/**", "/environment").permitAll()
				.antMatchers("/**").fullyAuthenticated().and().csrf().disable().exceptionHandling()
				.authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/ui/login"));

		// http.csrf().disable().authorizeRequests().antMatchers("/app/login",
		// "/VAADIN", "/VAADIN/*").permitAll()
		// .anyRequest().authenticated().and().formLogin().loginPage("/app/login").permitAll().and().logout()
		// .permitAll();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("user").password("password").roles("USER");
		UserDetailsService defaultUserDetailsService = auth.getDefaultUserDetailsService();

		System.out.println(defaultUserDetailsService);
	}
}