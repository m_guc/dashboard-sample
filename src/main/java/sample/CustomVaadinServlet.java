package sample;

import javax.servlet.ServletException;

import org.springframework.stereotype.Component;
import org.vaadin.spring.servlet.SpringAwareVaadinServlet;

@Component
public class CustomVaadinServlet extends SpringAwareVaadinServlet {

	@Override
	protected void servletInitialized() throws ServletException {
		super.servletInitialized();
		getService().addSessionInitListener(new DashboardSessionInitListener());
	}
}
